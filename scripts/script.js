

 
// on click accordion from culture and values
$('.culture-body').on('click', function() {
    $(this).siblings().removeClass('active');
    $(this).toggleClass('active');
});  

// Swiper carousal 
    var swiper = new Swiper(".swiper-container", {
        spaceBetween: 30,
        slidesPerView: 1,
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev"
        },
        
    });

  
